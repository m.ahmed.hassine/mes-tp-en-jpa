package fr.afcepf.ai107.jpa.program;

import java.util.Date;

import fr.afcepf.ai107.jpa.dao.CatDao;
import fr.afcepf.ai107.jpa.dao.CatIDao;
import fr.afcepf.ai107.jpa.dao.UserDao;
import fr.afcepf.ai107.jpa.dao.UserIDao;
import fr.afcepf.ai107.jpa.entity.Cat;
import fr.afcepf.ai107.jpa.entity.User;

public class TestGenericDao {

	public static void main(String[] args) {
		UserIDao userDao = new UserDao();
		User user = new User(null, "Dao", "Generic", "Dao", "Dao", null);
		userDao.add(user);
		user.setName("Nouveau nom");
		userDao.update(user);
		User user2 = userDao.getById(2);
		System.out.println(user2.getName());
//		userDao.delete(user);
		
//		CatIDao catDao = new CatDao();
//		Cat cat = new Cat(null, "Dao", "Generic", new Date(), "unePhoto.jpg", user, null);
//		catDao.add(cat);

	}

}
