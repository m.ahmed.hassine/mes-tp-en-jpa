package fr.afcepf.ai107.jpa.program;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import fr.afcepf.ai107.jpa.entity.Cat;
import fr.afcepf.ai107.jpa.entity.User;

public class TestCascade {
	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;
		
		try {
			//EntityManagerFactory est une factory qui permet d'instancier un EntityManager (équivalent de
			//la session Hibernate) en parsant notre persistence.xml
			emf = Persistence.createEntityManagerFactory("PremierJPA");
			em = emf.createEntityManager();
			em.getTransaction().begin();
			System.out.println("Configuration OK");
			
//			User user = new User(null, "Tutu", "de Tutu", "Tutu", "Tutu", null);
//			List<Cat> cats = new ArrayList<Cat>();
//			Cat cat = new Cat(null, "Félix", "Persan", new Date(), "felix.jpg", user);
//			Cat cat2 = new Cat(null, "Cachou", "Blue Russian", new Date(), "cachou.jpg", user);
//			Cat cat3 = new Cat(null, "Krys", "Bengal", new Date(), "krys.jpg", user);
//			cats.add(cat);
//			cats.add(cat2);
//			cats.add(cat3);
//			user.setCats(cats);
//			em.persist(user);
			
			//Cascade sur un Delete
			User user2 = em.find(User.class, 2);
			em.remove(user2);
			
			em.getTransaction().commit();
		}catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
		}finally {
			em.close();
			emf.close();
		}
		System.exit(0);
	}

}
