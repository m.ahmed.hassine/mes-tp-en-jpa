package fr.afcepf.ai107.jpa.dao;

import fr.afcepf.ai107.jpa.entity.User;

public interface UserIDao extends GenericIDao<User>{

}
