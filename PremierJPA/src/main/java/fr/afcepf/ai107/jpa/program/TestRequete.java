package fr.afcepf.ai107.jpa.program;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import fr.afcepf.ai107.jpa.entity.Cat;
import fr.afcepf.ai107.jpa.entity.User;

public class TestRequete {

	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		EntityManagerFactory emf = null;
		EntityManager em = null;

		try {
			//EntityManagerFactory est une factory qui permet d'instancier un EntityManager (équivalent de
			//la session Hibernate) en parsant notre persistence.xml
			emf = Persistence.createEntityManagerFactory("PremierJPA");
			em = emf.createEntityManager();
			em.getTransaction().begin();
			
			//Je veux tous les chats
			//En SQL
			Query querySQL = em.createNativeQuery("SELECT * FROM cAt");
			List<Object[]> catsSQL = querySQL.getResultList();
			for (Object[] objects : catsSQL) {
				System.out.println(objects[2] );
			}
			
			//En JPQL
			Query queryJPQL = em.createQuery("SELECT c FROM Cat c");
			List<Cat> catsJPQL = queryJPQL.getResultList();
			for (Cat cat : catsJPQL) {
				System.out.println("En JPQL " + cat.getName());
			}
			
			//Je veux le nom de l'utilisateur qui possède le chat qui a l'id n°1
			
			//En SQL
			Query querySQL2 = em.
					createNativeQuery("SELECT user.name FROM user INNER JOIN "
							+ "cat on user.id = cat.user_id where cat.id = 1");
			String name = (String) querySQL2.getSingleResult();
			System.out.println("Methode 1: " + name);
			
			//Avec le CRUD JPA			
			Cat cat = em.find(Cat.class, 1);
			System.out.println("Methode 2: " + cat.getUser().getName());
			
			//En JPQL sans jointure
			Query queryJPQL2 = em.createQuery("SELECT c.user.name FROM Cat c WHERE c.id = 1");
			String name2 = (String) queryJPQL2.getSingleResult();
			System.out.println("Méthode 3: " + name2);
			
			//EN JPQL avec jointure
			Query queryJPQL3 = em.createQuery("SELECT u.name FROM User u INNER JOIN u.cats c where c.id = 1");
			String name3 = (String) queryJPQL3.getSingleResult();
			System.out.println("Méthode 4: " + name3);
			
			
			
			

			em.getTransaction().commit();
		}catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
		}finally {
			em.close();
			emf.close();
		}
	}

}
