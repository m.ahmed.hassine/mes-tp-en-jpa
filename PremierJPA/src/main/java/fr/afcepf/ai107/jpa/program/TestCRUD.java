package fr.afcepf.ai107.jpa.program;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import fr.afcepf.ai107.jpa.entity.User;

public class TestCRUD {

	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;
		
		try {
			//EntityManagerFactory est une factory qui permet d'instancier un EntityManager (équivalent de
			//la session Hibernate) en parsant notre persistence.xml
			emf = Persistence.createEntityManagerFactory("PremierJPA");
			em = emf.createEntityManager();
			em.getTransaction().begin();
			System.out.println("Configuration OK");
			
			//Création d'un User
			User user = new User(null, "Java", "Persistence", "JPA", "JPA", null);
			em.persist(user);
			
			//Rechercher un user par son Id
			User user2 = em.find(User.class, 2);
			System.out.println("Le user trouvé s'appelle " + user2.getName());
			
			//Update un user
			user.setName("Jakarta");
			
			//Supprimer un user
			em.remove(user);
			
			em.getTransaction().commit();
		}catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
		}finally {
			em.close();
			emf.close();
		}

	}

}
