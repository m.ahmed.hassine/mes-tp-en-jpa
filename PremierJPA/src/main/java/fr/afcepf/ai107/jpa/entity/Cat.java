package fr.afcepf.ai107.jpa.entity;

import java.util.Date;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="cat")
public class Cat {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column (name = "id")
    private Integer id;
    @Column (name = "name")
    private String name;
    @Column (name = "race")
    private String race;
    @Column (name = "birth")
    private Date birth;
    @Column (name = "photo")
    private String photo;
    @ManyToOne
    @JoinColumn(referencedColumnName = "id")
    private User user;
    
    @OneToMany (mappedBy = "cat", cascade = CascadeType.ALL)
    private Set<Toy> toys;
   
    
    public Cat(Integer id, String name, String race, Date birth, String photo, User user, Set<Toy> toys) {
        super();
        this.id = id;
        this.name = name;
        this.race = race;
        this.birth = birth;
        this.photo = photo;
        this.user = user;
        this.toys = toys;
    }
    public Cat() {
        super();
        // TODO Auto-generated constructor stub
    }
    public String getPhoto() {
        return photo;
    }
    public void setPhoto(String photo) {
        this.photo = photo;
    }
   
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public String getRace() {
        return race;
    }
    public void setRace(String race) {
        this.race = race;
    }
    public Date getBirth() {
        return birth;
    }
    public void setBirth(Date birth) {
        this.birth = birth;
    }
    public User getUser() {
        return user;
    }
    public void setUser(User user) {
        this.user = user;
    }
    public Set<Toy> getToys() {
        return toys;
    }
    public void setToys(Set<Toy> toys) {
        this.toys = toys;
    }
    
}
