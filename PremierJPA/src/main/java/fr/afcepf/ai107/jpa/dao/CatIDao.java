package fr.afcepf.ai107.jpa.dao;

import fr.afcepf.ai107.jpa.entity.Cat;

public interface CatIDao extends GenericIDao<Cat> {

}
