package fr.afcepf.ai107.jpa.program;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import fr.afcepf.ai107.jpa.entity.User;

public class TestFetch {
	public static void main(String[] args) {
		EntityManagerFactory emf = null;
		EntityManager em = null;
		User user1 = null;
		try {
			//EntityManagerFactory est une factory qui permet d'instancier un EntityManager (équivalent de
			//la session Hibernate) en parsant notre persistence.xml
			emf = Persistence.createEntityManagerFactory("PremierJPA");
			em = emf.createEntityManager();
			em.getTransaction().begin();
			System.out.println("Configuration OK");

			//Dans le contexte d'une méthode de DAO
//			user1 = em.find(User.class, 1);
			
			//Première possibilité pour charger notre entité User en cats: une référence explicite
			//à un traitement sur la liste esclave
//			user1.getCats().size();
			
			//Deuxième Méthode qui est recommandée!
			String JPQL = "SELECT u FROM User u INNER JOIN FETCH u.cats c WHERE u.id=1";
			Query query = em.createQuery(JPQL);
			user1 = (User) query.getSingleResult();
			

			em.getTransaction().commit();
		}catch (Exception e) {
			em.getTransaction().rollback();
			e.printStackTrace();
		}finally {
			em.close();
			emf.close();
		}
		//Que reste t il en mémoire lorsque la transaction est close ?
		System.out.println(user1.getName());
		System.out.println(user1.getCats().size());
	}

}
