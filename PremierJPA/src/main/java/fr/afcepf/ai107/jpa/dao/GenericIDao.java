package fr.afcepf.ai107.jpa.dao;

public interface GenericIDao<T> {
	T add(T t);
	boolean delete(T t);
	T update(T t);
	T getById(int i);

}
