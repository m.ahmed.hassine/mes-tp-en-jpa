package fr.afcepf.ai107.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "toy")
public class Toy {
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    
    private String libelle;
    
    private String couleur;
    
    public Toy(Integer id, String libelle, String couleur, Cat cat) {
        super();
        this.id = id;
        this.libelle = libelle;
        this.couleur = couleur;
        this.cat = cat;
    }

    public Toy() {
        super();
        // TODO Auto-generated constructor stub
    }

    @ManyToOne
    @JoinColumn (referencedColumnName = "id")
    private Cat cat;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLibelle() {
        return libelle;
    }

    public void setLibelle(String libelle) {
        this.libelle = libelle;
    }

    public String getCouleur() {
        return couleur;
    }

    public void setCouleur(String couleur) {
        this.couleur = couleur;
    }

    public Cat getCat() {
        return cat;
    }

    public void setCat(Cat cat) {
        this.cat = cat;
    }
    
    
}
