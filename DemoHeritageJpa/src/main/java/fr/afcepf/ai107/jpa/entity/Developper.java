package fr.afcepf.ai107.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="Developper")
public class Developper extends Employee {

	private static final long serialVersionUID = 1L;
	
	private String language;

	public Developper() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Developper(Integer id, String name, String surname) {
		super(id, name, surname);
		// TODO Auto-generated constructor stub
	}

	public Developper(Integer id, String name, String surname, String language) {
		super(id, name, surname);
		this.language = language;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}
	
}
