package fr.afcepf.ai107.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="Guitar")
@PrimaryKeyJoinColumn(name="id")
public class Guitar extends MusicInstrument {

	private static final long serialVersionUID = 1L;
	
	private Boolean hasVibrato;

	
	public Guitar() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Guitar(Integer id, String name, Float price) {
		super(id, name, price);
		// TODO Auto-generated constructor stub
	}

	public Guitar(Integer id, String name, Float price, Boolean hasVibrato) {
		super(id, name, price);
		this.hasVibrato = hasVibrato;
	}

	public Boolean getHasVibrato() {
		return hasVibrato;
	}

	public void setHasVibrato(Boolean hasVibrato) {
		this.hasVibrato = hasVibrato;
	}
	
	

}
