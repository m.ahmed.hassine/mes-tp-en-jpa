package fr.afcepf.ai107.jpa.program;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import fr.afcepf.ai107.jpa.entity.Administrator;
import fr.afcepf.ai107.jpa.entity.Bass;
import fr.afcepf.ai107.jpa.entity.Customer;
import fr.afcepf.ai107.jpa.entity.Developper;
import fr.afcepf.ai107.jpa.entity.Employee;
import fr.afcepf.ai107.jpa.entity.Guitar;
import fr.afcepf.ai107.jpa.entity.MusicInstrument;
import fr.afcepf.ai107.jpa.entity.Tester;
import fr.afcepf.ai107.jpa.entity.User;

public class TestHeritage {

    @SuppressWarnings("unchecked")
	public static void main(String[] args) {
       
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("HeritagePU");
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        System.out.println("Connection bdd ok");
        
//        TEST SINGLE_TABLE
        User user = new User(null, "User", "User", "user", "user");
        em.persist(user);
        Customer Customer = new Customer(null, "Customer", "Customer", "Customer", "Customer", 7979);
        em.persist(Customer);
        Administrator admin = new Administrator(null, "Admin", "Admin", "admin", "adminr", 4242);
        em.persist(admin);
        
        Query userQuery = em.createQuery("SELECT u FROM User u");
        List<User> users = userQuery.getResultList();
        for (User userData : users) {
			System.out.println(userData.getName() + ": " + userData.getClass());
		}
        
        Query CustomerQuery = em.createQuery("SELECT c FROM Customer c");
        List<Customer> Customers = CustomerQuery.getResultList();
        for (Customer customerData : Customers) {
			System.out.println(customerData.getName() + ": " + customerData.getClass());
		}
        
        Query adminQuery = em.createQuery("SELECT a FROM Administrator a");
        List<Administrator> admins = adminQuery.getResultList();
        for (Administrator adminData : admins) {
			System.out.println(adminData.getName() + ": " + adminData.getClass());
		}
        
        
//        TEST JOINED
        MusicInstrument instrument = new MusicInstrument(null, "Bidule", 0f);
        em.persist(instrument);
       
        Guitar guitar = new Guitar(null, "Ibanez RG-220", 342.79f, true);
        em.persist(guitar);
        
        Bass bass = new Bass(null, "Fender Jazz Bass", 1442.54f, false);
        em.persist(bass);
        
      Query instrumentQuery = em.createQuery("SELECT i FROM MusicInstrument i");
      List<MusicInstrument> instruments = instrumentQuery.getResultList();
      for (MusicInstrument instrumentData : instruments) {
			System.out.println(instrumentData.getName() + ": " + instrumentData.getClass());
		}
      
      Query guitarQuery = em.createQuery("SELECT g FROM Guitar g");
      List<Guitar> guitars = guitarQuery.getResultList();
      for (Guitar guitarData : guitars) {
			System.out.println(guitarData.getName() + ": " + guitarData.getClass());
		}
      
      Query bassQuery = em.createQuery("SELECT b FROM Bass b");
      List<Bass> basses = bassQuery.getResultList();
      for (Bass bassData : basses) {
			System.out.println(bassData.getName() + ": " + bassData.getClass());
		}
      
//        TEST TABLE_PER_CLASS
      
        Employee emp = new Employee(null, "Employee", "Employee");
        em.persist(emp);
        
        Developper dev = new Developper(null, "Dave", "Dev", "Java");
        em.persist(dev);
        
        Tester test = new Tester(null, "Test", "Test", "Squash");
        em.persist(test);
        
      Query empQuery = em.createQuery("SELECT e FROM Employee e");
      List<Employee> emps = empQuery.getResultList();
      for (Employee empData : emps) {
			System.out.println(empData.getName() + ": " + empData.getClass());
		}
      
      Query devQuery = em.createQuery("SELECT d FROM Developper d");
      List<Developper> devs = devQuery.getResultList();
      for (Developper devData : devs) {
			System.out.println(devData.getName() + ": " + devData.getClass());
		}
      
      Query testQuery = em.createQuery("SELECT t FROM Tester t");
      List<Tester> tests = testQuery.getResultList();
      for (Tester testData : tests) {
			System.out.println(testData.getName() + ": " + testData.getClass());
		}
        
        em.getTransaction().commit();
        em.close();
        emf.close();
        System.exit(0);

    }

}
