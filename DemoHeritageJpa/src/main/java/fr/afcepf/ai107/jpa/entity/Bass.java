package fr.afcepf.ai107.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="Bass")
@PrimaryKeyJoinColumn(name="id")
public class Bass extends MusicInstrument {

	private static final long serialVersionUID = 1L;
	
	private Boolean isActiv;

	
	public Bass() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Bass(Integer id, String name, Float price) {
		super(id, name, price);
		// TODO Auto-generated constructor stub
	}

	public Bass(Integer id, String name, Float price, Boolean isActiv) {
		super(id, name, price);
		this.isActiv = isActiv;
	}

	public Boolean getIsActiv() {
		return isActiv;
	}

	public void setIsActiv(Boolean isActiv) {
		this.isActiv = isActiv;
	}
}