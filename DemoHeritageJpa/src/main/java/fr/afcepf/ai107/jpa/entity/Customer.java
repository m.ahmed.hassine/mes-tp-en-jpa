package fr.afcepf.ai107.jpa.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table (name="Client")
@DiscriminatorValue("Client")
public class Customer extends User{


	private static final long serialVersionUID = 1L;

	private Integer cardCode;

	public Customer() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Customer(Integer id, String name, String surname, String login, String password) {
		super(id, name, surname, login, password);
		// TODO Auto-generated constructor stub
	}

	public Customer(Integer id, String name, String surname, String login, String password, Integer cardCode) {
		super(id, name, surname, login, password);
		this.cardCode = cardCode;
	}

	public Integer getCardCode() {
		return cardCode;
	}

	public void setCardCode(Integer cardCode) {
		this.cardCode = cardCode;
	}
}

