package fr.afcepf.ai107.jpa.entity;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table (name="Administrator")
@DiscriminatorValue("Admin")
public class Administrator extends User{


	private static final long serialVersionUID = 1L;

	private Integer adminCode;

	public Administrator() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Administrator(Integer id, String name, String surname, String login, String password) {
		super(id, name, surname, login, password);
		// TODO Auto-generated constructor stub
	}

	public Administrator(Integer id, String name, String surname, String login, String password, Integer adminCode) {
		super(id, name, surname, login, password);
		this.adminCode = adminCode;
	}

	public Integer getAdminCode() {
		return adminCode;
	}

	public void setAdminCode(Integer adminCode) {
		this.adminCode = adminCode;
	}
}