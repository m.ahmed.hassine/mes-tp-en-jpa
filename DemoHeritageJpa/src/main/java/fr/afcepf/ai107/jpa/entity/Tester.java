package fr.afcepf.ai107.jpa.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="Tester")
public class Tester extends Employee {

	private static final long serialVersionUID = 1L;
	
	private String testingTool;

	public Tester() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Tester(Integer id, String name, String surname) {
		super(id, name, surname);
		// TODO Auto-generated constructor stub
	}

	public Tester(Integer id, String name, String surname, String testingTool) {
		super(id, name, surname);
		this.testingTool = testingTool;
	}

	public String getTestingTool() {
		return testingTool;
	}

	public void setTestingTool(String testingTool) {
		this.testingTool = testingTool;
	}

}
