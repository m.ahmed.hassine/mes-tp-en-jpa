package fr.afcepf.ai107.jpa.entity;

import java.io.Serializable;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;


@Entity
@Inheritance(strategy=InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="utilisator_type")
@DiscriminatorValue(value="User")
public class User implements Serializable{

	private static final long serialVersionUID = 1L;
		@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        private Integer id;
       
        private String name;
        
        private String surname;
        
        private String login;
        
        private String password;
     
        public User(Integer id, String name, String surname, String login, String password) {
            super();
            this.id = id;
            this.name = name;
            this.surname = surname;
            this.login = login;
            this.password = password;
        }
        public User() {
            super();
            // TODO Auto-generated constructor stub
        }
        public Integer getId() {
            return id;
        }
        public void setId(Integer id) {
            this.id = id;
        }
        public String getName() {
            return name;
        }
        public void setName(String name) {
            this.name = name;
        }
        public String getSurname() {
            return surname;
        }
        public void setSurname(String surname) {
            this.surname = surname;
        }
        public String getLogin() {
            return login;
        }
        public void setLogin(String login) {
            this.login = login;
        }
        public String getPassword() {
            return password;
        }
        public void setPassword(String password) {
            this.password = password;
        }      
}
